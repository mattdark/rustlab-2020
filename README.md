# RustLab 2020

## Create and Deploy your first Rust Web App
Creating web apps is one way you can use Rust. With frameworks like Rocket you can create fast and secure applications. Then, deploying your project is a task you can complete with any CI/CD tool that supports Rust and the platform you choose. This workshop will guide you through the whole process.


Creating web apps is one way you can use Rust. There are frameworks like Rocket that you can use for creating fast and secure applications. After that, deploying your project to a cloud platform like Heroku is a task you can complete with any CI/CD tool that supports Rust and the platform you choose. GitLab CI is a tool that works well with Rust projects.

This workshop will take you through the process of creating your first Rust web app and deploying to a platform like Heroku. 

Outline: 

    1. Basics of Rocket 
    2. Directory structure 
    3. Templates 
    4. Routing 
    5. Your first web app 
    6. A new git repository 
    7. Configuration 
    8. Heroku 
    9. GitLab CI 
    10. Up and running

### Presentation
Available on: [drive.google.com/file/d/1g2l1IghdrKtEVp107c-kWDTKcUHDyoYi/view?usp=sharing](https://drive.google.com/file/d/1g2l1IghdrKtEVp107c-kWDTKcUHDyoYi/view?usp=sharing)

### Snippets
- Hello, RustLab! --> [gitlab.com/mattdark/rustlab-2020/-/snippets/2028029](https://gitlab.com/mattdark/rustlab-2020/-/snippets/2028029)
- Routes --> [gitlab.com/mattdark/rustlab-2020/-/snippets/2028033](https://gitlab.com/mattdark/rustlab-2020/-/snippets/2028033)
- Dynamic Paths --> [gitlab.com/mattdark/rustlab-2020/-/snippets/2028035](https://gitlab.com/mattdark/rustlab-2020/-/snippets/2028035)
- Redirect --> [gitlab.com/mattdark/rustlab-2020/-/snippets/2028036](https://gitlab.com/mattdark/rustlab-2020/-/snippets/2028036)
- Static Files --> [gitlab.com/mattdark/rustlab-2020/-/snippets/2028040](https://gitlab.com/mattdark/rustlab-2020/-/snippets/2028040)
- Welcome RustLab --> [gitlab.com/mattdark/rustlab-2020/-/snippets/2028046](https://gitlab.com/mattdark/rustlab-2020/-/snippets/2028046)
- Business Card --> [gitlab.com/mattdark/rustlab-2020/-/snippets/2028074](https://gitlab.com/mattdark/rustlab-2020/-/snippets/2028074)